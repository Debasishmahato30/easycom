<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [Home::class, 'index']);
Route::view('login', 'login');
Route::view('register', 'register');
Route::post('login', [Home::class, 'login']);
Route::get('logout', [Home::class, 'logout']);
Route::post('register', [Home::class, 'register']);
Route::get('products', [Home::class, 'products']);
Route::get('add-product', [Home::class, 'add_product']);
Route::get('products_list', [Home::class, 'products_list']);
Route::post('insert-product', [Home::class, 'insert_product']);
Route::get('test', [Home::class, 'test']);
