<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class Home extends Controller
{
    //
    function __construct()
    {
        // $common = new Common();
    }
    function index(Request $request){
        return view('login');
    }
    function login(Request $request){
        if($_POST){
            // print_r($request->email);exit;
            $request->validate([
                'email' => 'required',
                'password' => 'required'
            ]);
            $whr = array('email'=>$request->email, 'password'=>$request->password);
            
            $user = User::where($whr)->first();
            // print_r($a);exit;
            if(!empty($user)){
                $request->session()->put('userId', $user->id);
                $request->session()->put('userName', $user->name);
                // $request->session->put('userId', $user->id);
                // $request->session->put('userName', $user->name);
                return redirect('/products');
                
            }else{
                return back()->withErrors([
                    'email' => 'Invalid Credentials',
                ]);
            }
        }
    }
    // function register(){
    //     return view('register');
    // }
    function register(Request $request){
        if($_POST){
            $request->validate([
                'name' => 'required|max:50',
                'email' => 'required|unique:users|max:200',
                'password' => ['required',Password::min(8)
                ->letters()
                ->mixedCase()
                ->numbers()
                ->symbols()
                ]
            ]);
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->save();
            $id = $user->id;
            $request->session()->put('userId', $user->id);
            $request->session()->put('userName', $user->name);
            return redirect("/products");
        }
    }
    function products(Request $request){
        // echo $request->session()->get('userId');exit;
        if(empty($request->session()->get('userId'))){
            return redirect('/login');
        }

        // $data['products'] = Product::all();
        // foreach ($data['products'] as &$key) {
        //     $key['images'] = DB::table('product_images')->where('product_id', $key['id'])->get();
        // }
        return view('products_list');

    }

    function products_list(Request $request){
        // echo "<pre>";print_r($_GET);exit;
        $added_by = $request->session()->get('userId');
        $draw = $request->draw;
        $start = $request->start;
        $length = $request->length;
        $search_value = $request->search['value'];
        $order_by = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];
        $cols = ['id', 'name', 'size', 'dimension', 'status'];
        $all_prods = DB::table('products')->where('added_by', $added_by)->get();
        $num = count($all_prods);
        $num_filts = 0;
        $data = [];
        $filter_data = [];
        
        if($search_value == ''){
            $data = DB::table('products')->where('added_by', $added_by)->orderBy($cols[$order_by], $order_dir)->offset($start)->limit($length)->get();
            $filter_data = DB::table('products')->where('added_by', $added_by)->get();

        }else{
            $data = DB::table('products')
                     ->where('added_by', $added_by)
                     ->where('id', 'like', '%'.$search_value.'%')
                     ->orWhere('name', 'like', '%'.$search_value.'%')
                     ->orWhere('size', 'like', '%'.$search_value.'%')
                     ->orWhere('dimension', 'like', '%'.$search_value.'%')
                     ->orWhere('status', 'like', '%'.$search_value.'%')
                     ->offset($start)
                     ->limit($length)
                     ->orderBy($cols[$order_by], $order_dir)
                     ->get();
            $filter_data = DB::table('products')
            ->where('added_by', $added_by)
            ->where('id', 'like', '%'.$search_value.'%')
            ->orWhere('name', 'like', '%'.$search_value.'%')
            ->orWhere('size', 'like', '%'.$search_value.'%')
            ->orWhere('dimension', 'like', '%'.$search_value.'%')
            ->orWhere('status', 'like', '%'.$search_value.'%')
            ->get();

        }
        $num_filts = count($filter_data);
        $response = array();
        // echo "<pre>"
        foreach ($data as $key) {
            $temp = array();
            array_push($temp, $key->id);
            array_push($temp, $key->name);
            array_push($temp, $key->size);
            array_push($temp, trim(trim($key->dimension,"{"), "}"));
            array_push($temp, $key->status);
            array_push($response, $temp);
        }
        echo json_encode(['draw'=> (int)($draw), 'recordsFiltered'=> (int)($num_filts), 'recordsTotal'=> (int)($num), 'data'=> $response]);
    }

    function add_product(Request $req){
        if(empty($req->session()->get('userId'))){
            return redirect('/login');
        }
        return view('add_product');
    }
    function logout(Request $req){
        $req->session()->put('userId', "");
        $req->session()->put('userName', "");
        return redirect('/login');
    }
    function insert_product(Request $req){
        if(empty($req->session()->get('userId'))){
            return redirect('/login');
        }
        if($_POST){
            // return $req;
            // print_r($_FILES);exit;
            $added_by = $req->session()->get('userId');
            $req->validate([
                'name' => 'required|max:50',
                'length' => 'required|integer',
                'width' => 'required|integer',
                'height' => 'required|integer',
                'mrp' => 'required',
                'color' => 'required',
                'weight' => 'required'

            ]);
            $product = new Product();
            $length = $req->length;
            $width = $req->width;
            $height = $req->height;
            $weight = $req->weight;
            $size = !empty($req->size)?$req->size:"";
            $product->status = "Rejected";
            $product->added_by = $added_by;
            if($size != ""){
                if(ucfirst($size) == 'S'){
                    if($length == 8 && $height == 6 && $width == 7){
                        $product->status = "Approved";
                    }
    
                }elseif(ucfirst($size) == 'M'){
                    if($length == 9 && $height == 7 && $width == 8){
                        $product->status = "Approved";
                    }
                }elseif(ucfirst($size) == 'L'){
                    if($length == 10 && $height == 7 && $width == 12){
                        $product->status = "Approved";
                    }
                }elseif(ucfirst($size) == 'XL'){
                    if($length == 12 && $height == 9 && $width == 13){
                        $product->status = "Approved";
                    }
                }elseif(ucfirst($size) == 'XXL'){
                    if($length == 12 && $height == 11 && $width == 15){
                        $product->status = "Approved";
                    }
                }
            }else{
                if($weight < 500){
                    if($length == 8 && $height == 6 && $width == 7){
                        $product->status = "Approved";
                    }
                }elseif($weight < 1000){
                    if($length == 10 && $height == 7 && $width == 12){
                        $product->status = "Approved";
                    }
                }else{
                    if($length == 12 && $height == 11 && $width == 15){
                        $product->status = "Approved";
                    }
                }
            }
            $product->name = $req->name;
            $product->size = $req->size;
            $product->dimension = json_encode(array("L" =>$length, "H" => $height, "W" => $width, "Weight" => $weight));
            $product->color = $req->color;
            $product->mrp = $req->mrp;
            $product->save();
            $product_id = $product->id;
            // $product_id=1;
            // print_r($req->file('images'));

            if($req->hasfile('images'))
            {
                // echo "IMGS";exit;
                $insert = array();
                foreach($req->file('images') as $key => $file)
                {
                    $path = 'images';
                    $imageName = uniqid().'_'.time().'.'.$file->extension(); 
                    $k = $file->move(public_path($path), $imageName);
                    $insert[$key]['product_id'] = $product_id;
                    $insert[$key]['file_path'] = $path.'/'.$imageName;
                    $insert[$key]['created_at'] = date('Y-m-d H:i:s');
                    
    
                }
                // print_r($insert);
                if(!empty($insert)){
                    DB::table('product_images')->insert($insert);
                }
                
            }
            // echo "NOIMGS";exit;
            return redirect('/products');
        }
        return view('add_product');
    }

    function test(){
        $s=array('S', 'M', 'L');
        for($i=1;$i<=100;$i++){
            $k =rand(0,2);
            $product = new Product();
            
            $product->name = "Product".$i;
            $product->size = $s[$k];
            $product->dimension = "10, 6, 3, 800";
            $product->color = "#FFCCGG";
            $product->mrp = 670.90;
            $product->save();
        }
    }
}
