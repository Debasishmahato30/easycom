<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" >
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <title><?= $title?></title>
</head>
<!------ Include the above in your HEAD tag ---------->
<header>
    <div class="container-fluid p-2">
        <?php if(!empty(session()->get('userId'))){?>
        <div class="row">
            <div class="col-6 col-sm-12">
                <a href="/products" class="btn btn-outline-primary" style="border-radius: 40px;">Home</a>
                <a href="/add-product" class="btn btn-outline-primary" style="border-radius: 40px;">Add New Products</a>
            
            
                <a href="" class="btn btn-outline-primary" style="border-radius: 40px;">Hello <?= session()->get('userName')?></a>
                <a href="/logout" class="btn btn-outline-primary" style="border-radius: 40px;">Logout</a>
            </div>
        </div>
        <?php }?>

    </div>
</header>

