<x-header componentName="Product List" />

<div class="container-fluid">
  
  <table class="table table-bordered" id="category_table">
    <thead>
      <tr>
        <th scope="col">Product ID</th>
        <th scope="col">Name</th>
        <th scope="col">Size</th>
        <th scope="col">Dimension</th>
        <th scope="col">Status</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
</div>
<script>
//   $(document).ready(function() {
      $('#category_table').DataTable( {
          "processing": true,
          "serverSide": true,
          "ajax": "/products_list"
      } );
//   } );
</script>