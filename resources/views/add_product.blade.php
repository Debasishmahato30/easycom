<x-header componentName="Add Product" />
<body>
    <div id="add_new_product">
        <h3 class="text-center text-white pt-5">Add Product</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="add_product_form" class="form" action="/insert-product" method="post" enctype="multipart/form-data">
                            @csrf
                            <h3 class="text-center text-info">Add Product</h3>
                            <div class="form-group">
                                <label for="name" class="text-info">Name:</label><br>
                                <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Name">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="mrp" class="text-info">MRP:</label><br>
                                <input type="number" step="0.01" name="mrp" id="mrp" class="form-control @error('mrp') is-invalid @enderror" value="{{ old('mrp') }}" placeholder="MRP">
                                
                                @error('mrp')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="size" class="text-info">Size:</label><br>
                                <input type="text" name="size" id="size" class="form-control @error('size') is-invalid @enderror" value="{{ old('size') }}" placeholder="Size(S, M, L, XL, XXL)">
                                
                                @error('size')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="color" class="text-info">Color:</label><br>
                                <input style="height: 50px;width: 60px;" type="color" name="color" id="color" class="form-control @error('color') is-invalid @enderror" value="{{ old('size') }}">
                                
                                @error('color')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="length" class="text-info">Length:</label><br>
                                <input type="text" name="length" id="length" class="form-control @error('length') is-invalid @enderror" value="{{ old('length') }}" placeholder="Length(cms)">
                                {{-- <span style="font-size: 10px;"><b>Dimension Format(Comma Seperated): Length(cms), Width(cms), Height(cms), Weight(gms)</b></span> --}}
                                @error('length')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="height" class="text-info">Height:</label><br>
                                <input type="text" name="height" id="height" class="form-control @error('height') is-invalid @enderror" value="{{ old('height') }}" placeholder="Height(cms)">
                                {{-- <span style="font-size: 10px;"><b>Dimension Format(Comma Seperated): Length(cms), Width(cms), Height(cms), Weight(gms)</b></span> --}}
                                @error('height')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="width" class="text-info">Width:</label><br>
                                <input type="text" name="width" id="width" class="form-control @error('width') is-invalid @enderror" value="{{ old('width') }}" placeholder="Width(cms)">
                                {{-- <span style="font-size: 10px;"><b>Dimension Format(Comma Seperated): Length(cms), Width(cms), Height(cms), Weight(gms)</b></span> --}}
                                @error('width')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="weight" class="text-info">Weight:</label><br>
                                <input type="text" name="weight" id="weight" class="form-control @error('weight') is-invalid @enderror" value="{{ old('weight') }}" placeholder="Weight(gms)">
                                {{-- <span style="font-size: 10px;"><b>Dimension Format(Comma Seperated): Length(cms), Width(cms), Height(cms), Weight(gms)</b></span> --}}
                                @error('weight')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="images" class="text-info">Image(s):</label><br>
                                <input type="file" name="images[]" multiple id="images" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
